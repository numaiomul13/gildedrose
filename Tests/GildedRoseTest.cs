﻿using NUnit.Framework;
using System.Collections.Generic;
using GildedRose;
using GildedRose.Items;

namespace Tests {
	[TestFixture]
	public class GildedRoseTest {
		[Test]
		public void ItemCreation() {
			IList<Item> Items = new List<Item> { new Item { Name = "foo", SellIn = 0, Quality = 0 } };
			Assert.AreEqual("foo", Items[0].Name);
			Assert.AreEqual(0, Items[0].SellIn);
			Assert.AreEqual(0, Items[0].Quality);
		}
		[Test]
		public void StandardItem() {
			IList<Item> Items = new List<Item> {
				new StandardItem{ Name = "foo", SellIn = 0, Quality = 10 },
				new StandardItem{ Name = "foo", SellIn = 1, Quality = 10 },
				new StandardItem{ Name = "foo", SellIn = 0, Quality = 0 },
				new StandardItem{ Name = "foo", SellIn = 1, Quality = 0 }

			};
			Inventory inv = new Inventory(Items);
			inv.UpdateQuality();
			//we test time passage
			Assert.AreEqual(-1, Items[0].SellIn);
			//we test quality and it's lower limit
			Assert.AreEqual(8, Items[0].Quality);
			Assert.AreEqual(9, Items[1].Quality);
			Assert.AreEqual(0, Items[2].Quality);
			Assert.AreEqual(0, Items[3].Quality);
		}

		[Test]
		public void ConjuredItem() {
			IList<Item> Items = new List<Item> {
				new ConjuredItem{ Name = "foo", SellIn = 0, Quality = 10 },
				new ConjuredItem{ Name = "foo", SellIn = 1, Quality = 10 },
				new ConjuredItem{ Name = "foo", SellIn = 0, Quality = 0 },
				new ConjuredItem{ Name = "foo", SellIn = 1, Quality = 0 }

			};
			Inventory inv = new Inventory(Items);
			inv.UpdateQuality();
			//we test time passage
			Assert.AreEqual(-1, Items[0].SellIn);
			//we test quality and it's lower limit
			Assert.AreEqual(6, Items[0].Quality);
			Assert.AreEqual(8, Items[1].Quality);
			Assert.AreEqual(0, Items[2].Quality);
			Assert.AreEqual(0, Items[3].Quality);
		}
		[Test]
		public void FineItem() {
			IList<Item> Items = new List<Item> {
				new FineItem{ Name = "foo", SellIn = 0, Quality = 10 },
				new FineItem{ Name = "foo", SellIn = 1, Quality = 10 },
				new FineItem{ Name = "foo", SellIn = 0, Quality = 0 },
				new FineItem{ Name = "foo", SellIn = 1, Quality = 0 }

			};
			Inventory inv = new Inventory(Items);
			inv.UpdateQuality();
			//we test time passage
			Assert.AreEqual(-1, Items[0].SellIn);
			//we test quality and it's lower limit
			Assert.AreEqual(12, Items[0].Quality);
			Assert.AreEqual(11, Items[1].Quality);
			Assert.AreEqual(2, Items[2].Quality);
			Assert.AreEqual(1, Items[3].Quality);
		}
		[Test]
		public void LegendaryItem() {
			IList<Item> Items = new List<Item> {
				new LegendaryItem{ Name = "foo", SellIn = 0, Quality = 10 },
				new LegendaryItem{ Name = "foo", SellIn = 1, Quality = 10 },
				new LegendaryItem{ Name = "foo", SellIn = 0, Quality = 0 },
				new LegendaryItem{ Name = "foo", SellIn = 1, Quality = 0 },
				new LegendaryItem{ Name = "foo", SellIn = 0, Quality = 90 },
				new LegendaryItem{ Name = "foo", SellIn = 1, Quality = 90 }

			};
			Inventory inv = new Inventory(Items);
			inv.UpdateQuality();
			//we test time passage
			Assert.AreEqual(0, Items[0].SellIn);
			//we test quality and it's lower limit
			Assert.AreEqual(10, Items[0].Quality);
			Assert.AreEqual(10, Items[1].Quality);
			Assert.AreEqual(0, Items[2].Quality);
			Assert.AreEqual(0, Items[3].Quality);
			Assert.AreEqual(90, Items[4].Quality);
			Assert.AreEqual(90, Items[5].Quality);
		}
		[Test]
		public void TimeSensitiveItem() {
			IList<Item> Items = new List<Item> {
				new TimeSensitiveItem{ Name = "foo", SellIn = 11, Quality = 20 },
				new TimeSensitiveItem{ Name = "foo", SellIn = 11, Quality = 50 },
				new TimeSensitiveItem{ Name = "foo", SellIn = 10, Quality = 20 },
				new TimeSensitiveItem{ Name = "foo", SellIn = 10, Quality = 50 },
				new TimeSensitiveItem{ Name = "foo", SellIn = 5, Quality =  20},
				new TimeSensitiveItem{ Name = "foo", SellIn = 5, Quality = 50 },
				new TimeSensitiveItem{ Name = "foo", SellIn = 0, Quality = 20 },
				new TimeSensitiveItem{ Name = "foo", SellIn = 0, Quality = 50 }

			};
			Inventory inv = new Inventory(Items);
			inv.UpdateQuality();
			//we test time passage
			Assert.AreEqual(10, Items[0].SellIn);
			//we test quality and it's lower limit
			Assert.AreEqual(21, Items[0].Quality);
			Assert.AreEqual(50, Items[1].Quality);
			Assert.AreEqual(22, Items[2].Quality);
			Assert.AreEqual(50, Items[3].Quality);
			Assert.AreEqual(23, Items[4].Quality);
			Assert.AreEqual(50, Items[5].Quality);
			Assert.AreEqual(0, Items[6].Quality);
			Assert.AreEqual(0, Items[7].Quality);
		}
	}
}
