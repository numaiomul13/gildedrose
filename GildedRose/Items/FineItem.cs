﻿using System;
namespace GildedRose.Items {
	public class FineItem : ItemAdaptor {
		const int QUALITY_GAINED = 1;
		protected override void PassDayInt() {
			IncQuality(QUALITY_GAINED);
			if (SellIn < 0) {
				IncQuality(QUALITY_GAINED);
			}

		}
	}
}