﻿using System;
namespace GildedRose.Items {
	public class TimeSensitiveItem : ItemAdaptor {
		const int QUALITY_INC = 1;
		protected override void PassDayInt() {
			if (SellIn < 0) {
				Quality = 0;
			} else {
				if (SellIn < 5) {
					IncQuality(QUALITY_INC);
				}
				if (SellIn < 10) {
					IncQuality(QUALITY_INC);
				}
				IncQuality(QUALITY_INC);
			}
		}
	}
}
