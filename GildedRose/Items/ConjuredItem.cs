﻿using System;
namespace GildedRose.Items {
    public class ConjuredItem : ItemAdaptor {
		const int QUALITY_LOST = 2;
		protected override void PassDayInt() {
			DecQuality(QUALITY_LOST);
			if (SellIn < 0) {
				DecQuality(QUALITY_LOST);
			}

		}
	}
}
