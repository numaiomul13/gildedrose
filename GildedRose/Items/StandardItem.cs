﻿using System;
namespace GildedRose.Items {
	public class StandardItem :ItemAdaptor {
        const int QUALITY_LOST = 1;
        protected override void PassDayInt() {
            DecQuality(QUALITY_LOST);
            if (SellIn < 0) {
                DecQuality(QUALITY_LOST);
            }

        }
    }
}