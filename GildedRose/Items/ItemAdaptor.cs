﻿using System;
namespace GildedRose.Items {
	public abstract class ItemAdaptor : Item {
		const int MAX_QUALITY = 50;
		const int MIN_QUALITY = 0;

		public void PassDay(int _value = 1) {
			for (int i = 0; i < _value; i++) {
				SellIn--;
				PassDayInt();
			}
		}
		protected abstract void PassDayInt();
		public void IncQuality(int _value) {
			Quality += _value;
			Quality = Math.Min(MAX_QUALITY, Quality);
		}
		public void DecQuality(int _value) {
			Quality -= _value;
			Quality = Math.Max(MIN_QUALITY, Quality);
		}
	}
}
